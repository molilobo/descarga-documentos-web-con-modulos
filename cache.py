#!/usr/bin/python3

from robot import Robot

class Cache:
    def __init__(self):
        self.cache= {}

    def retrieve(self,url):
        if not url in self.cache:
            bot = Robot(url=url)
            self.cache[url] = bot

    def show(self,url):
        self.retrieve(url)
        print(self.content)
    def content(self,url):
        self.retrieve(url)
        return self.cache[url].content()
    def showAll(self):
        for url in self.cache:
            print(url)


if __name__ == '__main__':
    c = Cache()
    c.retrieve('http://gsyc.urjc.es/')
    c.show('https://www.aulavirtual.urjc.es')
    c.showAll()